
#include <Servo.h>  // Include Servo Library

#define Rx 17 // DOUT to pin 17
#define Tx 16 // DIN  to pin 16

// Declare onboard LED pins
#define ONBOARD_LED_RED 45
#define ONBOARD_LED_GREEN 46
#define ONBOARD_LED_BLUE 44

// Declare button pin
#define BUTTON_PIN 9

// Declare QTI pins
#define RIGHT_QTI_PIN 52
#define CENTER_QTI_PIN 53
#define LEFT_QTI_PIN 51

// Declare Servo pins
#define SERVO_LEFT_PIN 12
#define SERVO_RIGHT_PIN 11

// Declare piezo pins
#define PIEZO_PIN 7

// Declare white/black min/max
#define WHITE_MAX_LEFT 40
#define WHITE_MAX_CENTER 40
#define WHITE_MAX_RIGHT 50

// Declare time (in ms) to pause and then continue after crossing a hash
#define PAUSE_TIME 500
#define STRAIGHT_TIME 150

// Declare the number of samples of white before stopping the robot
#define WHITE_REPEAT_THRESHOLD 20

// Declare the length of time (ms) when making a sharp turn
#define SHARP_TURN_LENGTH 250

// Declare the number of times to spam the final score
#define SPAM_COUNT 1000

#define LOOP_SPEED 50 // Hz; number of times loop runs per second

// Declare colors
#define RGB new int[3] 
#define COLOR_RED RGB {255, 0, 0}
#define COLOR_GREEN RGB {0, 255, 0}
#define COLOR_BLUE RGB {0, 0, 255}
#define COLOR_YELLOW RGB {255, 255, 0}
#define COLOR_PURPLE RGB {255, 0, 255}
#define COLOR_BLACK RGB {0, 0, 0}

// Declare 7-segment display pins
#define BOTH_ONES 5
#define RIGHT_ZERO 4
#define LEFT_ZERO 3

// ========================================================================== //
// NATIONAL ANTHEM MUSIC AKA RICKROLL
// ========================================================================== //

#define  a3f    208     // 208 Hz
#define  b3f    233     // 233 Hz
#define  b3     247     // 247 Hz
#define  c4     261     // 261 Hz MIDDLE C
#define  c4s    277     // 277 Hz
#define  e4f    311     // 311 Hz    
#define  f4     349     // 349 Hz 
#define  a4f    415     // 415 Hz  
#define  b4f    466     // 466 Hz 
#define  b4     493     // 493 Hz 
#define  c5     523     // 523 Hz 
#define  c5s    554     // 554 Hz
#define  e5f    622     // 622 Hz  
#define  f5     698     // 698 Hz 
#define  f5s    740     // 740 Hz
#define  a5f    831     // 831 Hz 

#define rest    -1

volatile int beatlength = 100; // determines tempo
float beatseparationconstant = 0.3;

int threshold;

int a = 4; // part index
int b = 0; // song index
int c = 0; // lyric index

boolean flag;
String lastLyrics = "";

// Parts 1 and 2 (Intro)

int song1_intro_melody[] =
{c5s, e5f, e5f, f5, a5f, f5s, f5, e5f, c5s, e5f, rest, a4f, a4f};

int song1_intro_rhythmn[] =
{6, 10, 6, 6, 1, 1, 1, 1, 6, 10, 4, 2, 10};

// Parts 3 or 5 (Verse 1)

int song1_verse1_melody[] =
{ rest, c4s, c4s, c4s, c4s, e4f, rest, c4, b3f, a3f,
  rest, b3f, b3f, c4, c4s, a3f, a4f, a4f, e4f,
  rest, b3f, b3f, c4, c4s, b3f, c4s, e4f, rest, c4, b3f, b3f, a3f,
  rest, b3f, b3f, c4, c4s, a3f, a3f, e4f, e4f, e4f, f4, e4f,
  c4s, e4f, f4, c4s, e4f, e4f, e4f, f4, e4f, a3f,
  rest, b3f, c4, c4s, a3f, rest, e4f, f4, e4f
};

int song1_verse1_rhythmn[] =
{ 2, 1, 1, 1, 1, 2, 1, 1, 1, 5,
  1, 1, 1, 1, 3, 1, 2, 1, 5,
  1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 3,
  1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 4,
  5, 1, 1, 1, 1, 1, 1, 1, 2, 2,
  2, 1, 1, 1, 3, 1, 1, 1, 3
};

const char* lyrics_verse1[] =
{ "We're ", "no ", "strangers ", "", "to ", "love ", "", "",
  "You ", "know ", "the ", "rules ", "and ", "so ", "do ", "I",
  "A ", "full ", "commitment's ", "", "", "what ", "I'm ", "thinking ", "", 
  "of", "",
  "You ", "wouldn't ", "", "get ", "this ", "from ", "any ", "", "other ", "", 
  "guy",
  "I ", "just ", "wanna ", "", "tell ", "you ", "how ", "I'm ", "feeling", "",
  "Gotta ", "", "make ", "you ", "understand", "", ""
};

// Parts 4 or 6 (Chorus)

int song1_chorus_melody[] =
{ b4f, b4f, a4f, a4f,
  f5, f5, e5f, b4f, b4f, a4f, a4f, e5f, e5f, c5s, c5, b4f,
  c5s, c5s, c5s, c5s,
  c5s, e5f, c5, b4f, a4f, a4f, a4f, e5f, c5s,
  b4f, b4f, a4f, a4f,
  f5, f5, e5f, b4f, b4f, a4f, a4f, a5f, c5, c5s, c5, b4f,
  c5s, c5s, c5s, c5s,
  c5s, e5f, c5, b4f, a4f, rest, a4f, e5f, c5s, rest
};

int song1_chorus_rhythmn[] =
{ 1, 1, 1, 1,
  3, 3, 6, 1, 1, 1, 1, 3, 3, 3, 1, 2,
  1, 1, 1, 1,
  3, 3, 3, 1, 2, 2, 2, 4, 8,
  1, 1, 1, 1,
  3, 3, 6, 1, 1, 1, 1, 3, 3, 3, 1, 2,
  1, 1, 1, 1,
  3, 3, 3, 1, 2, 2, 2, 4, 8, 4
};

const char* lyrics_chorus[] =
{ "Never ", "", "gonna ", "", "give ", "you ", "up",
  "Never ", "", "gonna ", "", "let ", "you ", "down", "", "",
  "Never ", "", "gonna ", "", "run ", "around ", "", "", "", "and ", "desert ", 
  "", "you",
  "Never ", "", "gonna ", "", "make ", "you ", "cry",
  "Never ", "", "gonna ", "", "say ", "goodbye ", "", "", "",
  "Never ", "", "gonna ", "", "tell ", "a ", "lie ", "", "", "and ", "hurt ", 
  "you"
};

// count for seeing if it rounded loop once
int roundedloop = 0;

#include <Wire.h>
#include "Adafruit_TCS34725.h"

/* Example code for the Adafruit TCS34725 breakout library */

/* Connect SCL    to analog 5
   Connect SDA    to analog 4
   Connect VDD    to 3.3V DC
   Connect GROUND to common ground */

/* Initialise with default values (int time = 2.4ms, gain = 1x) */
// Adafruit_TCS34725 tcs = Adafruit_TCS34725();

/* Initialise with specific int time and gain values */
Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_614MS, 
    TCS34725_GAIN_1X);

int loopDelay = 1000 / LOOP_SPEED; // Calculate loop delay from loop speed

enum STATES { NONE, CHARACTER_SENT, CHARACTER_RECEIVED }; // Define an 
                                                          // emueration of 
                                                          // possible states

String STATE_DESCRIPTIONS[] = { "NONE", "CHARACTER_SENT", // Provide human-
    "CHARACTER_RECEIVED"};                                // readable 
                                                          // descriptions for
                                                          // debugging

int NEXT_STATE_AFTER[] { NONE, NONE, NONE }; // What to do after each state 
                                             // terminates

int STATE_TIMEOUTS[] { -1, 1000, 1000 };     // The timeout for transitioning
                                             // from one state to another
                                             // (-1 = never transition)

enum QTI_PINS { LEFT, CENTER, RIGHT }; // Define an enumeration of QTI pins

enum DIRECTIONS { DIR_LEFT, DIR_STRAIGHT, DIR_RIGHT, DIR_PAUSE, // Define an 
    DIR_SHARP_LEFT, DIR_SHARP_RIGHT, DIR_BACKWARDS, DIR_STOP }; // enumeration
                                                                // of possible 
                                                                // directions
                                                
Servo servoLeft;    // Declare Left and Right Servos
Servo servoRight;

int whiteCount = 0;

void setup() {

    Serial.begin(9600);  // Communicate with the host computer at 9600 baud
    Serial2.begin(9600); // Communicate with the XBee antenna at 9600 baud
    Serial3.begin(9600); // Communicate with the LCD at 9600 baud
    
    servoLeft.attach(SERVO_LEFT_PIN);
    servoRight.attach(11);

    // Initialize pins to output/input
    pinMode(BUTTON_PIN, INPUT);

    pinMode(BOTH_ONES, OUTPUT);
    pinMode(RIGHT_ZERO, OUTPUT);
    pinMode(LEFT_ZERO, OUTPUT);

    pinMode(RIGHT_QTI_PIN, INPUT);
    pinMode(CENTER_QTI_PIN, INPUT);
    pinMode(LEFT_QTI_PIN, INPUT);

    pinMode(ONBOARD_LED_RED, OUTPUT);
    pinMode(ONBOARD_LED_GREEN, OUTPUT);
    pinMode(ONBOARD_LED_BLUE, OUTPUT);

    pinMode(PIEZO_PIN, OUTPUT);

    pinMode(10, INPUT);

    // Clear all displays 
    setOnboardLight(COLOR_BLACK);         // Turn off onboard LED
    displayNumber(-1);                    // Turn off 7-segment displays
    delay(100);                           // Give LCD screen time to initialize
    displayText("Following line", "..."); // Show initial text on the LCD screen

    // Sound a brief tone to indicate the start of the trial
    tone(PIEZO_PIN, 440, 500);
}

int stateClockMillis = 0;            // Keep track of how long we have been in a 
                                     // given state
                                     
long lastMillis = 0;                 // Used to keep track of elapsed time
int currentState = NONE;             // Initial state is "not doing anything"

long sensorReadings[] = { 0, 0, 0 }; // This array is used to globally share the 
                                     // sensor readings from the left, center, 
                                     // and right QTIs

void loop() {

    if (currentState != NONE) {                                 
        // If we are actively doing something
        Serial.println(stateClockMillis);
        if (stateClockMillis >= STATE_TIMEOUTS[currentState]) { 
            // If time has expired
            setState(NEXT_STATE_AFTER[currentState]); // Change state to 
                                                      // whatever would be next
        } else {
            long currentTime = millis();                    // Retrieve the 
                                                            // current time

            stateClockMillis += (currentTime - lastMillis); // Add elapsed time 
                                                            // since last loop

            lastMillis = currentTime;                       // Set last time to
                                                            // current time
        }
    }

    // ====================================================================== //
    // NOTE: the below communication code is no longer necessary! Is was only 
    // used for the communication "demo" preformed in IDCs 1, 2, and 3.
    // ====================================================================== //

    
    if (currentState == NONE && Serial2.available()) { 
        // If we received a character, and weren't in the process of 
        // doing something else

        while (Serial2.available()) Serial2.read(); // Read the character 
                                                    // (clear it from buffer)

        Serial.println("Received message");         // Print to Serial monitor
        setState(CHARACTER_RECEIVED);               // Change state to 
                                                    // "character received" - 
                                                    // prevents another 
                                                    // character from being sent 
                                                    // for 1 second

        setOnboardLight(COLOR_GREEN);               // Turn on the green light 
                                                    // (for 1s)
    }

    if (digitalRead(BUTTON_PIN) && currentState == NONE) { 
        // If someone pressed the button, and we weren't in the process of 
        // doing something else

        Serial.println("Sending message..."); // Print to Serial monitor
        setState(CHARACTER_SENT);             // Change state to "character 
                                              // sent" - prevents another 
                                              // character from being sent for 
                                              // 1 second

        setOnboardLight(COLOR_RED);           // Turn on the red light (for 1s)
        Serial2.print('~');                   // Send a character
    }

    // ====================================================================== //
    // CODE FOR COMMUNICATION AND SENSING
    // ====================================================================== //


    if (roundedloop == 1) { // When we cross the hash mark for the first time

        // Show text on the LCD
        displayText("Preparing to", "sense the color"); 

        // Drive to the center of the ring
        int count = 0; 
        backUp();
        delay(350);
        turnLeft();
        delay(1110);
        goStraight();
        delay(1050);
        stopMotors();
        roundedloop = 0; // Reset loop count (technically unused, because 
                         // we reset the bot manually after each trial)

        int ourScore = 0;

        // Sense the correct court color (twice, because sometimes the RGB 
        // sensor is wrong on the first initialization) and then display the 
        // result using the onboard LED, LCD screen and 7-segment display
        while (count < 2) {
            stopMotors();
            getSensorReadings();
            int courtLux = checkLight();
            if (courtLux > 2300) {
                // WHITE
                setOnboardLight(COLOR_RED);
                displayNumber(0);
                ourScore = 0;
                displayText("Detected color", "is WHITE");
            } else if (courtLux <600) {
                // BLACK
                setOnboardLight(COLOR_BLUE);
                displayNumber(2);
                ourScore = 2;
                displayText("Detected color", "is BLACK");
            } else {
                // GRAY
                setOnboardLight(COLOR_GREEN);
                displayNumber(1);
                ourScore = 1;
                displayText("Detected color", "is GRAY");
            } 
            count += 1;
        }

        // Face the front of the classroom
        turnRight();
        delay(1110);
        stopMotors();

        // ================================================================== //
        // COMMUNICATION WITH OTHER BOTS
        // ================================================================== //

        long startTime = millis();         // Keep track of elapsed time
        bool notReceivedAllSignals = true; // Loop invariant: still waiting for 
                                           // one or more bots
        int signals[] = {-1, -1, -1, -1};  // Array used to store signals
        String botScores = "";             // Text used to display on LCD

        while(millis() - startTime < 120000 && notReceivedAllSignals) { 
            // While the time (two minutes) has not yet expired,
            // and we are still waiting on signals from one or more bots

            while (Serial2.available()) {
                // Read incoming signals
                char c = Serial2.read();
                Serial.println(c);
                int ASCII = (int) c;         // Determine ASCII value of char
                int value = ASCII - 97;      // Determine 0-based letter index 
                int score = value % 3;       // Determine indicated score (0-2)
                int botID = (value-score)/3; // Determine bot ID (0 through 3)
                signals[botID] = score;      // Store the saved score
            }

            // Tentatively assume we have received a signal from everyone
            notReceivedAllSignals = false;   
            int botReceivedCount = 1;
            botScores = "";
            // Note, signals is an array of bytes, therefore length = size
            for (int i = 0; i < 4; i++) {
                if (signals[i] == -1) {
                    // Take note if there is any bot for which we have not
                    // yet received a signal
                    notReceivedAllSignals = true;
                    botScores += "X"; // Used to indicate on the LCD that 
                                      // we are still missing a score
                } else {
                    botReceivedCount++;
                    botScores += String(signals[i]); // Add the given score to 
                                                     // the display string
                }
                botScores += ", ";
            }

            botScores += String(ourScore); // Add our score to display string

            if (millis() % 1000 == 0) {
                // Update the text on the LCD screen
                int remainingTime = 120 - ((millis() - startTime) / 1000);
                String statusText = String(botReceivedCount);
                statusText += "/5 bots, T-";
                statusText += String(remainingTime);
                statusText += "s";
                displayText(botScores, statusText);
            }
        }

        int totalScore = ourScore;

        for (int i = 0; i < 4; i++) {
            totalScore += max(0, signals[i]);
        }

        int spamCounter = 0;
        int finalScoreResult = totalScore % 3;

        while (spamCounter++ < SPAM_COUNT) {
            Serial2.print(finalScoreResult);
            delay(1);
        }

        //setOnboardLight(COLOR_PURPLE);
        displayNumber(finalScoreResult);
        displayText(botScores, "Final result: " + String(finalScoreResult));

        switch(finalScoreResult) {
            case 2:
                while(true) play();
                break;
            case 1:
                doLightShow();
                break;
            case 0:
                dance();
                break;
        }

        while (true) {
        }
        
    }

    // ====================================================================== //
    // THE PROGRAM WILL EVENTUALLY TERMINATE HERE!!!
    // ====================================================================== //

    // Determine the appropriate action based on the QTI sensors
    int intendedDirection = getIntendedDirection();

    switch (intendedDirection) {
        case DIR_LEFT:
            turnLeft();
            break;
        case DIR_RIGHT:
            turnRight();
            break;
        case DIR_STRAIGHT: 
            goStraight();
            break;
        case DIR_PAUSE:
            stopMotors();
            delay(PAUSE_TIME);
            goStraight();
            delay(STRAIGHT_TIME);
            roundedloop += 1;
            break;
        case DIR_SHARP_LEFT:
            turnLeftSharp();
            delay(SHARP_TURN_LENGTH);
            break;
        case DIR_SHARP_RIGHT:
            turnRightSharp();
            delay(SHARP_TURN_LENGTH);
            break;
        case DIR_BACKWARDS:
            backUp();
            break;
        case DIR_STOP:
        default:
            stopMotors();
    }

    // NOTE: We previously checked the value of the RGB sensor in each loop;
    // however, this takes a significant amount of time, so currently pin 10 
    // is tied to ground to disable this feature.
    if (digitalRead(10)) checkLight();

    delay(loopDelay); // Wait for the previously calculated amount of time

} // END LOOP CODE /////////////////////////////////////////////////////////////

void setState(int state) {
    currentState = state; // Set the current state to `state`
    stateClockMillis = 0; // Reset the state clock
    lastMillis = millis();
    clearLeds(); // Turn off all the onboard LED
}

void clearLeds() {
    // Turn off the onboard LED
    setOnboardLight(COLOR_BLACK);
}

// ========================================================================== //
// QTI SENSOR CODE
// ========================================================================== //

long RCtime(int sensPin){

   long result = 0;

   pinMode(sensPin, OUTPUT);       // make pin OUTPUT
   digitalWrite(sensPin, HIGH);    // make pin HIGH to discharge capacitor
   delay(1);                       // wait a  ms to make sure cap is discharged
   
   pinMode(sensPin, INPUT);        // turn pin into an input
   digitalWrite(sensPin, LOW);     // turn pullups off

   while(digitalRead(sensPin)){    // wait for pin to go low
      result++;
   }

   return result;                   // report results
}

int getIntendedDirection() {
    getSensorReadings(); // Refresh the QTI values

    // Check if each of the values is above the threshold
    bool leftIsBlack = (sensorReadings[LEFT] > WHITE_MAX_LEFT);
    bool centerIsBlack = (sensorReadings[CENTER] > WHITE_MAX_CENTER);
    bool rightIsBlack = (sensorReadings[RIGHT] > WHITE_MAX_RIGHT);
    
    if (sensorReadings[LEFT] > 500 && sensorReadings[CENTER] > 500 && 
        sensorReadings[RIGHT] > 500) return DIR_STOP; // This checks if the 
                                                      // robot has been picked
                                                      // up off of the table

    if (leftIsBlack && rightIsBlack && centerIsBlack) return DIR_PAUSE;

    if (!leftIsBlack && !rightIsBlack && !centerIsBlack) {
        if (whiteCount < WHITE_REPEAT_THRESHOLD) {
            // Drive forward briefly to search for the line
            whiteCount += 1;
            return DIR_STRAIGHT;
        } else {
            // Back up; we must have missed the line
            return DIR_BACKWARDS;
        }
    } 

    // Don't drive forward again until we have either gone straight, forward,
    // or right
    whiteCount = 0;

    if (rightIsBlack) return DIR_RIGHT;
    if (leftIsBlack) return DIR_LEFT;

    return DIR_STRAIGHT;
}

void getSensorReadings() {
    // Obtain the RC time for each of the three QTI sensors
    sensorReadings[LEFT] = RCtime(LEFT_QTI_PIN);
    sensorReadings[CENTER] = RCtime(CENTER_QTI_PIN);
    sensorReadings[RIGHT] = RCtime(RIGHT_QTI_PIN);

    // Print the values to the serial monitor
    Serial.print(sensorReadings[LEFT]);
    Serial.print(","); 
    Serial.print(sensorReadings[CENTER]);
    Serial.print(","); 
    Serial.println(sensorReadings[RIGHT]);
}

// ========================================================================== //
// ONBOARD LED CODE
// ========================================================================== //

void setOnboardLight(int rgb[]) {
    int red = clamp(rgb[0], 0, 255);
    int green = clamp(rgb[1], 0, 255);
    int blue = clamp(rgb[2], 0, 255);

    analogWrite(ONBOARD_LED_RED, 255 - red);
    analogWrite(ONBOARD_LED_GREEN, 255 - green);
    analogWrite(ONBOARD_LED_BLUE, 255 - blue);
}

// Clamps an integer to be between `_min` and `_max`.
int clamp(int number, int _min, int _max) {
    return max(_min, min(number, _max));
}

// ========================================================================== //
// PARALLAX RGB SENSOR CODE
// ========================================================================== //

int checkLight() {
  
  uint16_t r, g, b, c, colorTemp, lux;

  tcs.getRawData(&r, &g, &b, &c);
  // colorTemp = tcs.calculateColorTemperature(r, g, b);
  colorTemp = tcs.calculateColorTemperature_dn40(r, g, b, c);
  lux = tcs.calculateLux(r, g, b);

  Serial.print("Color Temp: "); Serial.print(colorTemp, DEC); Serial.print(
      " K - ");
  Serial.print("Lux: "); Serial.print(lux, DEC); Serial.print(" - ");
  Serial.print("R: "); Serial.print(r, DEC); Serial.print(" ");
  Serial.print("G: "); Serial.print(g, DEC); Serial.print(" ");
  Serial.print("B: "); Serial.print(b, DEC); Serial.print(" ");
  Serial.print("C: "); Serial.print(c, DEC); Serial.print(" ");

  return lux;
}

// ========================================================================== //
// 7-SEGMENT DISPLAY CODE
// ========================================================================== //

void displayNumber(int number) {

    // Turn off the display
    digitalWrite(BOTH_ONES, HIGH);
    digitalWrite(RIGHT_ZERO, HIGH);
    digitalWrite(LEFT_ZERO, HIGH);

    if (number != -1) {
        // Turn on the "ones"
        digitalWrite(BOTH_ONES, LOW);
        if (number != 1) {
            // Turn on the right zero
            digitalWrite(RIGHT_ZERO, LOW);
        }
        if (number != 2) {
            // Turn on the left zero
            digitalWrite(LEFT_ZERO, LOW);
        }
    }
}

// ========================================================================== //
// PARALLAX LCD SCREEN CODE
// ========================================================================== //

void displayText(String line1, String line2) {
    byte moveToZero = 128;
    line1 = line1.substring(0, 16);
    line2 = line2.substring(0, 16);
    Serial3.write(moveToZero);
    int addToLine1 = 16 - line1.length();
    for (int i = 0; i < addToLine1; i++) line1 += " ";
    int addToLine2 = 16 - line2.length();
    for (int i = 0; i < addToLine2; i++) line2 += " ";
    Serial3.print(line1);
    Serial3.print(line2);
}

// ========================================================================== //
// MOTOR CONTROLLING CODE
// ========================================================================== //

void turnRight() {
    servoLeft.writeMicroseconds(1700);         // Left wheel counterclockwise
    servoRight.writeMicroseconds(1500);        // Right wheel stops
}

void turnLeft() {
    servoLeft.writeMicroseconds(1500);         // Left wheel stops
    servoRight.writeMicroseconds(1300);        // Right wheel clockwise
}

void turnRightSharp() {
    servoLeft.writeMicroseconds(1700);         // Left wheel counterclockwise
    servoRight.writeMicroseconds(1700);        // Right wheel counterclockwise
}

void turnLeftSharp() {
    servoLeft.writeMicroseconds(1300);         // Left wheel clockwise
    servoRight.writeMicroseconds(1300);        // Right wheel clockwise
}

void goStraight() {
    servoLeft.writeMicroseconds(1700);         // Left wheel counterclockwise
    servoRight.writeMicroseconds(1300);        // Right wheel clockwise
}

void backUp() {
    servoLeft.writeMicroseconds(1300);         // Left wheel clockwise
    servoRight.writeMicroseconds(1600);        // Right wheel counterclockwise 
                                               // (slightly slower to make an 
                                               // angled turn)
}

void stopMotors() {
    servoLeft.writeMicroseconds(1500);         // Left wheel stop
    servoRight.writeMicroseconds(1500);        // Right wheel stop
}

// ========================================================================== //
// NATIONAL ANTHEM CODE
// ========================================================================== //

void play() {
  int notelength;
  if (a == 1 || a == 2) {
    // intro
    notelength = beatlength * song1_intro_rhythmn[b];
    if (song1_intro_melody[b] > 0) {
      tone(PIEZO_PIN, song1_intro_melody[b], notelength);
    }
    b++;
    if (b >= sizeof(song1_intro_melody) / sizeof(int)) {
      a++;
      b = 0;
      c = 0;
    }
  }
  else if (a == 3 || a == 5) {
    // verse
    notelength = beatlength * 2 * song1_verse1_rhythmn[b];
    if (song1_verse1_melody[b] > 0) {
      Serial.print(lyrics_verse1[c]);
      displayText(lastLyrics, lyrics_verse1[c]);
      lastLyrics = lyrics_verse1[c];
      tone(PIEZO_PIN, song1_verse1_melody[b], notelength);
      c++;
    }
    b++;
    if (b >= sizeof(song1_verse1_melody) / sizeof(int)) {
      a++;
      b = 0;
      c = 0;
    }
  }
  else if (a == 4 || a == 6) {
    // chorus
    notelength = beatlength * song1_chorus_rhythmn[b];
    if (song1_chorus_melody[b] > 0) {
      Serial.print(lyrics_chorus[c]);
      displayText(lastLyrics, lyrics_chorus[c]);
      lastLyrics = lyrics_chorus[c];
      tone(PIEZO_PIN, song1_chorus_melody[b], notelength);
      c++;
    }
    b++;
    if (b >= sizeof(song1_chorus_melody) / sizeof(int)) {
      Serial.println("");
      a++;
      b = 0;
      c = 0;
    }
  }
  delay(notelength);
  noTone(PIEZO_PIN);
  delay(notelength * beatseparationconstant);
  if (a == 7) { // loop back around to beginning of song
    a = 1;
  }
}

// ========================================================================== //
// LIGHT SHOW CODE
// ========================================================================== //

void doLightShow() {
    while(1) {
        // Set the onboard light to various colors for 0.5s each (infinite loop)
        setOnboardLight(COLOR_RED);
        delay(500);
        setOnboardLight(COLOR_YELLOW);
        delay(500);
        setOnboardLight(COLOR_GREEN);
        delay(500);
        setOnboardLight(COLOR_BLUE);
        delay(500);
        setOnboardLight(COLOR_PURPLE);
        delay(500);
        setOnboardLight(COLOR_BLACK);
    }
}

// ========================================================================== //
// DANCE CODE
// ========================================================================== //

void dance() {
    
    // ====================================================================== //
    // DEFINE ALL THE DANCE MOVES
    // ====================================================================== //

    
    struct DanceMoves {
        
        static void spinRight() {
            // Spin right slowly, and then quickly
            turnRight();
            delay(1500);
            turnRightSharp();
            delay(3000);
        }

        static void spinLeft() {
            // Spin left slowly, and then quickly
            turnLeft();
            delay(1500);
            turnLeftSharp();
            delay(3000);
        }

        static void turnRight() {
            servoLeft.writeMicroseconds(1700);   // Left wheel counterclockwise
            servoRight.writeMicroseconds(1500);  // Right wheel stops
        }

        static void turnLeft() {
            servoLeft.writeMicroseconds(1500);   // Left wheel stops
            servoRight.writeMicroseconds(1300);  // Right wheel clockwise
        }

        static void turnRightSharp() {
            servoLeft.writeMicroseconds(1700);   // Left wheel counterclockwise
            servoRight.writeMicroseconds(1700);  // Right wheel counterclockwise
        }

        static void turnLeftSharp() {
            servoLeft.writeMicroseconds(1300);   // Left wheel clockwise
            servoRight.writeMicroseconds(1300);  // Right wheel clockwise
        }

        static void goStraight() {
            servoLeft.writeMicroseconds(1700);   // Left wheel counterclockwise
            servoRight.writeMicroseconds(1300);  // Right wheel clockwise
        }

        static void backUp() {
            servoLeft.writeMicroseconds(1300);   // Left wheel clockwise
            servoRight.writeMicroseconds(1600);  // Right wheel counterclockwise 
                                                 // (slightly slower to make an 
                                                 // angled turn)
        }

        static void backUpLeft() {
            servoLeft.writeMicroseconds(1600);   // Left wheel clockwise
            servoRight.writeMicroseconds(1300);
        }

        static void backStraight() {
            // Back up straight
            servoLeft.writeMicroseconds(1300);        
            servoRight.writeMicroseconds(1700);
        }
        
        static void backTurnRight() {
            // Back up to the right, and then turn right quickly
            backUp();
            delay(500);
            turnRightSharp();
            delay(1000);
        }
        
        static void backTurnLeft() {
            // Back up to the left, and then spin left quickly
            backUpLeft();
            delay(500);
            turnLeftSharp();
            delay(1000);
        }

        static void stopMotors() {
            servoLeft.writeMicroseconds(1500);         // Left wheel stop
            servoRight.writeMicroseconds(1500);        // Right wheel stop
        }

        static void finale() {
            // Wiggle-wobble slow
            for (int j = 0; j < 8; j++) {
                turnLeftSharp();
                delay(200);
                turnRightSharp();
                delay(200);
            }
            // Wiggle-wobble fast
            for (int j = 0; j < 16; j++) {
                turnLeftSharp();
                delay(100);
                turnRightSharp();
                delay(100);
            }
            // Do some spins to close out the dance
            turnRightSharp();
            delay(2000);
            backTurnLeft();
            turnLeftSharp();
            delay(4000);
            stopMotors();           
        }

    };

    // ====================================================================== //
    // DO THE ACTUAL DANCE!!!
    // ====================================================================== //
    
    DanceMoves::spinLeft();
    DanceMoves::backTurnLeft();
    DanceMoves::goStraight();
    delay(500);
    DanceMoves::spinRight();
    DanceMoves::backTurnRight();
    DanceMoves::backStraight();
    delay(500);
    DanceMoves::finale();
    
}