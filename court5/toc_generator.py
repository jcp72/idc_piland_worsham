ALLOW_INDENTED = True
LINE_LENGTH = 80
NUMBER_LENGTH = 3

with open("court5/court5.ino", "r") as file:
    file_lines = file.readlines()

toc = []

for index, line in enumerate(file_lines):
    char_pos = 0

    if ALLOW_INDENTED:
        while line[char_pos] == " ":
            char_pos += 1
    
    if line[char_pos:char_pos + 5] == "// ==":
        next_line = file_lines[index + 1][char_pos:]

        if next_line[0:2] == "//":
            full_entry = next_line[3:-1]  # Leave off initial "// ", trailing \n
            entry_name = ""

            for c in full_entry:
                if (not c.isupper() and not c.isalnum() and 
                        c != " " and c != "-"):
                    break
                entry_name += c

            toc.append((
                index + 4, # Line number
                entry_name, # Title
                int(char_pos / 2) # Indentation
            ))

output =  "TABLE OF CONTENTS\n" 
output += "=================\n\n"

for line, entry, indentation in toc:
    dots_needed = LINE_LENGTH - NUMBER_LENGTH - indentation - len(entry)
    output += ("-" * (indentation - 1) 
        + (" " if indentation > 0 else "")
        + f"{line:0{NUMBER_LENGTH}d}"
        + "." * dots_needed + entry) + "\n"  

with open("toc.txt", "w") as file:
    file.write(output) 
