#define Rx 17 // DOUT to pin 17
#define Tx 16 // DIN  to pin 16

// Declare onboard LED pins
#define ONBOARD_LED_RED 45
#define ONBOARD_LED_GREEN 46
#define ONBOARD_LED_BLUE 44
#define BUTTON_PIN 9

#define STATE_CLEAR_DELAY 1000 // milliseconds; delay before resetting state
#define LOOP_SPEED 50 // Hz 

// Declare colors
#define RGB new int[3] 
#define COLOR_RED RGB {255, 0, 0}
#define COLOR_GREEN RGB {0, 255, 0}
#define COLOR_YELLOW RGB {255, 255, 0}
#define COLOR_PURPLE RGB {255, 0, 255}
#define COLOR_BLACK RGB {0, 0, 0}

int loopDelay = 1000 / LOOP_SPEED;
enum STATES { NONE, CHARACTER_SENT, CHARACTER_RECEIVED };

void setup() {

    Serial.begin(9600);
    Serial2.begin(9600); // Communicate with the XBee antenna at 9600 baud
    pinMode(BUTTON_PIN, INPUT);

    pinMode(ONBOARD_LED_RED, OUTPUT);
    pinMode(ONBOARD_LED_GREEN, OUTPUT);
    pinMode(ONBOARD_LED_BLUE, OUTPUT);

    setOnboardLight(COLOR_BLACK);

    delay(500);
}

int stateClockMillis = 0; // Keep track of how long we have been in a given state
int currentState = NONE;

void loop() {
    
    if (currentState == NONE && Serial2.available()) { // If we received a character
        while (Serial2.available()) Serial2.read();
        Serial.println("Received message");
        setState(CHARACTER_RECEIVED);
        setOnboardLight(COLOR_GREEN);
    }

    if (currentState != NONE) {
        if (stateClockMillis >= STATE_CLEAR_DELAY) { // Check if time has expired
            setState(NONE);
        } else {
            stateClockMillis += loopDelay;
        }
    }

    if (digitalRead(BUTTON_PIN) && currentState == NONE) {
        Serial.println("Sending message...");
        setState(CHARACTER_SENT);
        setOnboardLight(COLOR_RED);
        Serial2.print('~');
    }

    delay(loopDelay);
}

void clearLeds() {
    // Set the red and green LED output pins to LOW
    setOnboardLight(COLOR_BLACK);
}

// Clamps an integer to be between `_min` and `_max`.
int clamp(int number, int _min, int _max) {
    return max(_min, min(number, _max));
}

void setOnboardLight(int rgb[]) {
    int red = clamp(rgb[0], 0, 255);
    int green = clamp(rgb[1], 0, 255);
    int blue = clamp(rgb[2], 0, 255);

    analogWrite(ONBOARD_LED_RED, 255 - red);
    analogWrite(ONBOARD_LED_GREEN, 255 - green);
    analogWrite(ONBOARD_LED_BLUE, 255 - blue);
}

void setState(int state) {
    currentState = state;
    stateClockMillis = 0;
    clearLeds();
}
